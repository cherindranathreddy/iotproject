import datetime

# Establishing device connection with IOT core
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
myMQTTClient = AWSIoTMQTTClient("sensor1")
myMQTTClient.configureEndpoint("a2ljlk9sudb600-ats.iot.ap-south-1.amazonaws.com", 8883)
myMQTTClient.configureCredentials(r"C:\Users\cheri\OneDrive\Desktop\projectIOT\Certificates\root.pem", 
                                  r"C:\Users\cheri\OneDrive\Desktop\projectIOT\Certificates\sensor1.private.key", 
                                  r"C:\Users\cheri\OneDrive\Desktop\projectIOT\Certificates\sensor1.cert.pem")
myMQTTClient.connect()

curr = datetime.datetime.now()
sensorData = '{"timestamp":'+'"'+str(curr)+'",'+'"name":'+'"sensor000"'+','+'"temperature":'+'"48"'+'}'

myMQTTClient.publish("sensor/device1/tempStatus",sensorData, 0)

print("done publishing")
myMQTTClient.disconnect()