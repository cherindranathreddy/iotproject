import boto3

# Stores the data from sensor into dynamodb table
def my_dbUtility(name,timestamp,temperature):
    db = boto3.resource('dynamodb')
    table = db.Table("testiot")

    table.put_item(
        Item={
            'timestamp':str(timestamp),
            'name':str(name),
            'temprature':str(temperature),
        }
    )

# Sends email with alert message
def my_snsUtility(name,timestamp,temperature):
    client = boto3.client('sns')
    response = client.publish(
    TopicArn='arn:aws:sns:ap-south-1:210667013755:sensorTemp',
    Message='alert: temperature is {} at {} at {}'.format(temperature,name,timestamp),
    )

# Function pre-processing the data
def lambda_function(event,context):
    tempf = (1.8)*int(event['temperature']) + 32
    
    #function to store data in dynamoDB
    my_dbUtility(event['name'],event['timestamp'],tempf)

    if int(event['temperature']) > 50:
        #function to email notification
        my_snsUtility(event['name'],event['timestamp'],event['temperature'])


# event = {'name':'sensor221',
#     'timestamp':'25/01/2001 7:52',
#     'temperature': '56' 
# }

# context = {}

# lambda_function(event, context)




    
    



