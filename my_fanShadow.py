import json

# Establishing shadow connection with IOT core
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTShadowClient
myShadowClient = AWSIoTMQTTShadowClient("fan1")
myShadowClient.configureEndpoint("a2ljlk9sudb600-ats.iot.ap-south-1.amazonaws.com", 8883)
myShadowClient.configureCredentials(r"C:\Users\cheri\OneDrive\Desktop\projectIOT\Certificates\fanRootCA1.pem",
                                    r"C:\Users\cheri\OneDrive\Desktop\projectIOT\Certificates\fan1-private.pem.key",
                                    r"C:\Users\cheri\OneDrive\Desktop\projectIOT\Certificates\fan1-certificate.pem.crt")
myShadowClient.connect()

# Establishing MQTT connection from device shadow to subscribe and publish to a topic
myMQTTClient = myShadowClient.getMQTTConnection()

myJSONPayloadON = '{"state":'+'{"desired":'+'{"name":'+'"fan1"'+","+'"engine":'+'"ON"'+'}}}'     
myJSONPayloadOFF = '{"state":'+'{"desired":'+'{"name":'+'"fan1"'+","+'"engine":'+'"OFF"'+'}}}'     

def myCustomCallbackGET(client,userdata,message):
    print("get callback called")
    print(message.payload)

def myCustomCallback(client,userdata,message):
    print("callback called")
    data = json.loads(message.payload)
    temperature = data['temperature']

    # If Temperature is greater than threshold(50) then fan will be turned ON  
    if(int(temperature) > 50):
        print("temperature is more than 50")
        myMQTTClient.publish("$aws/things/fan1/shadow/update", str(myJSONPayloadON), 0)
        print("fan is on")
    
    # If Temperature is less than threshold(50) then fan will be turned OFF 
    if(int(temperature) <= 50):
        print("temperature is less than or equal to 50")
        myMQTTClient.publish("$aws/things/fan1/shadow/update", str(myJSONPayloadOFF), 0)
        print("fan is off")


myMQTTClient.subscribe("sensor/device1/temperature",1,myCustomCallback)
print('waiting for callback.click to continue')
x = input()

myMQTTClient.unsubscribe("sensor/device1/temperature")
myMQTTClient.disconnect()